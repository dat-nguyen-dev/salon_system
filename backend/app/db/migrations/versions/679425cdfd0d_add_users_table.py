"""add_users_table

Revision ID: 679425cdfd0d
Revises: 712a8b51fa5d
Create Date: 2021-04-26 14:55:04.529550

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic
revision = '679425cdfd0d'
down_revision = None
branch_labels = None
depends_on = None

import enum

class Role(enum.Enum):
    manager = 1
    employee = 2
    customer = 3


def upgrade():
    op.create_table(
        "users",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("create_at", sa.DateTime, nullable=False),
        sa.Column("name", sa.Text, nullable=False),
        sa.Column("code", sa.Text, nullable=False, index=True, unique=True),
        sa.Column("birthday", sa.Date, nullable=True),
        sa.Column("description", sa.Text, nullable=True),
        sa.Column("address", sa.Text, nullable=True),
        sa.Column("phone_number", sa.String, nullable=False, unique=True),
        # sa.Column("roles", ChoiceType(Role)),
        sa.Column("date_joined", sa.Date, nullable=False),
    )


def downgrade():
    op.drop_table("users")
