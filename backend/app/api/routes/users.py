from typing import List, Optional
from datetime import datetime, date

from sqlalchemy.sql.sqltypes import Numeric, String
from sqlalchemy import update

from pydantic import BaseModel
from sqlalchemy.orm import Session
from ...core.base import SessionLocal, Base, engine
from ..models.users import Users

from fastapi import APIRouter, Depends

Base.metadata.create_all(bind=engine)


router = APIRouter()

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


class UserInfo(BaseModel):
    id: int
    create_at: datetime
    name: Optional[str]
    code: Optional[str]
    birthday: date
    description: Optional[str]
    address: Optional[str]
    phone_number: Optional[str]
    date_joined: date
    address: Optional[str]
    
    class Config:
        orm_mode = True


@router.get("/", response_model=List[UserInfo])
def get_all_customer(db: Session = Depends(get_db)) -> List[dict]:
    return get_all(db)

def get_all(db):
    return  db.query(Users).all()

class UserCreate(BaseModel):
    name: Optional[str]
    description: Optional[str]
    phone_number: Optional[str]
    code: Optional[str]
    address: Optional[str]
    birthday: Optional[date]
    date_joined: Optional[date]


class UserId(BaseModel):
    id: str

@router.post("/", response_model=UserId)
def create_customer(user: UserCreate, db: Session = Depends(get_db)):
    db_item = Users(**user.dict())
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return {'id': db_item.id}


@router.delete("/{user_id}")
def delete_customer(user_id: int, db: Session = Depends(get_db)):
    user = db.query(Users).get(user_id)
    db.delete(user)
    db.commit()
    return 


@router.get("/{user_id}", response_model=UserInfo)
def get_item_customer(user_id: int, db: Session = Depends(get_db)) -> dict:
    user = db.query(Users).get(user_id)
    return user


@router.patch("/{user_id}")
def update_item(user_id: str, user: UserCreate, db: Session = Depends(get_db)):
    # Update model class variable from requested fields 
    db_user = db.query(Users).filter(Users.id == user_id).one_or_none()
    if db_user is None:
        return None

    # Update model class variable from requested fields 
    for var, value in vars(user).items():
        setattr(db_user, var, value) if value else None
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user
    
