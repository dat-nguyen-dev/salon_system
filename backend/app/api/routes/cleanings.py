from typing import List, Optional

from sqlalchemy.sql.sqltypes import Numeric, String

from pydantic import BaseModel
from sqlalchemy.orm import Session
from ...core.base import SessionLocal, Base, engine
from ..models.test import Learnings

from fastapi import APIRouter, Depends

Base.metadata.create_all(bind=engine)


router = APIRouter()

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


class Learning(BaseModel):
    id: int
    name: str
    description: str
    cleaning_type: str
    price: float

    class Config:
        orm_mode = True


@router.get("/", response_model=List[Learning])
async def get_all_cleanings(db: Session = Depends(get_db)) -> List[dict]:
    cleanings = db.query(Learnings).all()

    return cleanings
