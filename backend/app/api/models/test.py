from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy import Column, Integer, Text, Numeric

from ...core.base import Base


class Learnings(Base):
    __tablename__ = "cleanings"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(Text, nullable=False, index=True)
    description = Column(Text, nullable=True)
    cleaning_type = Column(Text, nullable=False, server_default="spot_clean")
    price = Column(Numeric(10, 2), nullable=False)
