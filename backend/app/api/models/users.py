from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, DateTime, Date, Text
from sqlalchemy.orm import relationship
from datetime import datetime
import enum

from ...core.base import Base

class Role(enum.Enum):
    manager = 1
    employee = 2
    customer = 3

class Users(Base):
    __bind_key__ = 'users'
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    create_at = Column(DateTime, default=datetime.now())
    name = Column(String)
    code = Column(String)
    birthday = Column(Date)
    description = Column(Text)
    address = Column(Text)
    phone_number = Column(String, unique=True)
    # roles = Enum(Role)
    date_joined = Column(Date)
