// create table before migrate
docker-compose run --rm server alembic revision -m "name_file"

// runmigrate up
docker-compose run --rm server alembic upgrade head

// runmigrate down
docker-compose run --rm server alembic downgrade -(n)
